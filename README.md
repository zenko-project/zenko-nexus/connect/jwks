# Zenko Connect - JWKS Endpoint

JWKS endpoint for the OIDC Server Zenko Connect that give the key used

## Table of Contents

- [Release](#release)
- [Deploy](#deploy)


## Release

Changelog must be updated before each release using the [git-cliff tool](https://github.com/orhun/git-cliff)


## Deploy

Update the config file:
- [ ] The environment variable in the [k8s service file](k8s/service.yaml)

```sh
kube apply -f k8s/service.yaml
```

