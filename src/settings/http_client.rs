use std::time::Duration;

use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct HttpClientSetting {
    pub timeout: Option<Duration>
}
