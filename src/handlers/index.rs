use axum::{
    extract::{Query, State},
};
use std::sync::Arc;
use tracing::debug;

use crate::{app::state::AppState, dtos};

pub async fn handle(
    State(state): State<Arc<AppState>>,
    Query(query): Query<dtos::index::Query>,
) -> Result<(), ()> {
    debug!("Query: {:?}", query);

    Ok(())
}
